package com.devcamp.s50.jbr4_50;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class accountsController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getAccountApi(){
        Customer customer1 = new Customer(1, "Dao", 15);
        Customer customer2 = new Customer(2, "xuan", 40);
        Customer customer3 = new Customer(3, "duong", 50);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Account account1 = new Account(111, customer1);
        Account account2 = new Account(222, customer2, 120000);
        Account account3 = new Account(332, customer2, 300000);

        account1.withdraw(10000);
        account2.deposit(80000);
        account3.withdraw(150000);

        ArrayList<Account> AccountList = new ArrayList<>();
        AccountList.add(account1);
        AccountList.add(account2);
        AccountList.add(account3);

        return AccountList ;
    }
}
