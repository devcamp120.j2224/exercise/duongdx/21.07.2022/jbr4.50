package com.devcamp.s50.jbr4_50;

public class Account {
    private int id ;
    private Customer customer ;
    private double balance = 0.0 ;
    
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [balance=" + (double) Math.round(balance * 100) / 100 + ", customer=" + customer + ", id=" + id + "]";
    }

    public double deposit(double amount){
        return balance += amount ;
    }

    public double withdraw(double amount){
        if(balance >= amount){
            balance -= amount ;
        }else{
            new String("So Du Khong Du");
        }
        return balance ;
    }
}
